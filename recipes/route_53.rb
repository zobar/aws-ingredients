#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

packages = %w[build-essential libxslt1-dev]

#
# Packages needed to build the fog gem.
#
packages.each do |p|
  package p
end

#
# Chef likes installing chef_gems very early in the process, but we can't
# guarantee fog will compile unless we also have build-essential and
# libxslt1-dev.
#
chef_gem 'fog' do
  only_if "dpkg-query -W #{packages.join(' ')} > /dev/null 2>&1"
end

#
# This script makes a best effort to remove the CNAMEs if they still point to
# this node when it shuts down. This is not compatible with the -P option to
# <tt>knife ec2 server delete</tt>.
#
cookbook_file '/etc/init.d/route-53' do
  mode   0744
  source 'route-53'
end

#
# Register the shutdown script.
#
execute 'insserv:route-53' do
  command 'insserv route-53'
end

aws.route_53.hosted_zones.each do |hosted_zone, h|
  h.cnames.each do |cname, c|

    #
    # The service script fails if you try running it before the Chef run is over
    # (it can't get data from the server because it hasn't been pushed yet).
    # That means we have to register the CNAMEs here, too.
    #
    ruby_block "register_cname:#{cname}" do
      block do
        require 'fog'
        c.register!
      end
    end
  end
end

service 'route-53' do
  supports :'force-reload' => true,
           :restart        => true,
           :start          => true,
           :stop           => true
end
