#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

include_recipe 'ingredients'

#
# /etc/hostname should always contain the node's hostname.
#
file '/etc/hostname' do
  content ec2.hostname
  mode    0644

  action   :create_if_missing
  notifies :run, 'execute[hostname]', :immediately
end

#
# Sets the node's hostname from the contents of /etc/hostname.
#
execute 'hostname' do
  command 'hostname -F /etc/hostname'

  action :nothing
end

ec2.block_device_mappings.each do |name, device|
  case name
  when 'ami'

    #
    # The AMI block device is already mounted on /
    #
    link '/media/ami' do
      to '/'
    end

  when 'swap'

    #
    # Enable swapping on the block device named "swap"
    #
    execute 'swapon' do
      not_if "grep -Fq /dev/#{device} /proc/swaps"

      command "swapon /dev/#{device}"
    end

  else

    #
    # Directory to mount the block device on
    #
    directory "/media/#{name}" do
      mode 0755
    end

    #
    # Mount the block device
    #
    mount "/media/#{name}" do
      device "/dev/#{device}"
    end
  end
end
