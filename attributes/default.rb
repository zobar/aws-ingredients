#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

Ingredients.for_cookbook :aws do
  data_bag_item 'services', 'aws'

  data_bag_attribute :access_key_id
  data_bag_attribute :secret_access_key

  namespace :route_53 do
    def fog_dns
      return @fog if instance_variable_defined? :@fog
      @fog = Fog::DNS.new aws_access_key_id:     parent.access_key_id,
                          aws_secret_access_key: parent.secret_access_key,
                          provider:              :aws
    end

    named_collection :hosted_zones do
      def fog_zone
        return @fog_zone if instance_variable_defined? :@fog_zone
        @fog_zone = parent.fog_dns.zones.get name
      end

      named_collection :cnames do
        def deregister
          fog_record.destroy unless fog_record.nil?
        end

        def fog_record
          return @fog_record if instance_variable_defined? :@fog_record
          @fog_record = if !fog_record_for_any_host.nil? &&
              fog_record_for_any_host.value == [fqdn]
            fog_record_for_any_host
          end
        end

        def fog_record_for_any_host
          if instance_variable_defined? :@fog_record_for_any_host
            return @fog_record_for_any_host
          end
          @fog_record_for_any_host = parent.fog_zone.records.get name, 'CNAME'
        end

        def fqdn
          return @fqdn if instance_variable_defined? :@fqdn
          @fqdn = "#{ec2.public_hostname}."
        end

        def register!
          unless fog_record
            unless fog_record_for_any_host.nil?
              fog_record_for_any_host.destroy
            end
            parent.fog_zone.records.create name: name, type: 'CNAME',
                                           value: fqdn
          end
        end
      end
    end
  end
end

Ingredients.for_cookbook :ec2 do
  attribute :local_hostname
  attribute :public_hostname

  def block_device_mappings
    if instance_variable_defined? :@block_device_mappings
      return @block_device_mappings
    end
    @block_device_mappings = Mash.new
    config.each do |key, value|
      match = key.match /\Ablock_device_mapping_(?<name>.+)\Z/
      unless match.nil?
        name = match[:name]
        if value =~ /\Asd.+\Z/
          @block_device_mappings[name] = value.sub /\As/, 'xv'
        end
      end
    end
    @block_device_mappings
  end

  def hostname
    return @hostname if instance_variable_defined? :@hostname
    @hostname = local_hostname[/\A[^.]*/]
  end
end
